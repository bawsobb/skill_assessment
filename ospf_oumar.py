def calcul_contiguite_voisinage(nb_routeur):
    
    if nb_routeur <= 1:
        return 0
    else:
        # Pour chaque routeur, il peut établir une relation de voisinage
        # avec tous les autres routeurs, à l'exception de lui-même.
        return nb_routeur - 1

# Demande à l'utilisateur d'entrer le nombre de routeurs OSPF
nb_routeur = int(input("Entrez le nombre de routeurs OSPF : "))

# Utilisation de la fonction pour calculer le nombre de contiguïtés de voisinage
contiguites_voisinage = calcul_contiguite_voisinage(nb_routeur)
print("Nombre de contiguïtés de voisinage :", contiguites_voisinage)
